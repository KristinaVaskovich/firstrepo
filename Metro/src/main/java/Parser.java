import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Parser {
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    static final String GSON_PATH = "src\\main\\resources\\map.json";
    static List<Line> lines = new LinkedList<>();
    static Map<String, List<String>> stations = new TreeMap<>();
    static Document doc;
    static Metro metro = new Metro(lines, stations);

    static void createDoc(String url) throws IOException {
        Connection con = Jsoup.connect(url).maxBodySize(0);
        doc = con.get();
    }

    static void createJSONFile() throws IOException {
        FileWriter file = new FileWriter(GSON_PATH);
        file.write(GSON.toJson(metro));
        file.flush();
    }

    static void parseLines() {
        for (Element el : doc.select(".js-metro-line")) {
            String lineNumber = el.attr("data-line");
            Line line = new Line(lineNumber, el.text());
            lines.add(line);
        }
    }

    static void parseStations() {
        Elements namesOfStations = doc.getElementsByClass("js-metro-stations");
        for (Element el : namesOfStations) {
            Elements ch = el.children();
            String lineName = el.attr("data-line");
            for (Element e : ch) {
                String stationName = e.getElementsByClass("name").text();
                if (!stations.containsKey(lineName)) {
                    stations.put(lineName, new ArrayList<>());
                } else {
                    stations.get(lineName).add(stationName);
                }
            }
        }
    }

    static void jsonParser() throws Exception {
        FileReader reader = new FileReader(GSON_PATH);
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
        Map<String, List<String>> st = (Map<String, List<String>>) jsonObject.get("stations");
        for (String lineNumber : st.keySet()) {
            JSONArray stationsArray = (JSONArray) st.get(lineNumber);
            for (Line line : metro.getLines()) {
                if (line.getNumber().equals(lineNumber)) {
                    System.out.println("На линии номер " + lineNumber + " с названием " + line.getName() + " находится станций: " + stationsArray.size());
                }
            }
        }
    }
}