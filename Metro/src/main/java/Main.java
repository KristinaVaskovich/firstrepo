
public class Main {
    private static final String stringUrl = "https://www.moscowmap.ru/metro.html#lines";

    public static void main(String[] args) {
        try {
            Parser.createDoc(stringUrl);

            Parser.parseLines();
            Parser.parseStations();

            Parser.createJSONFile();
            System.out.println("JSON документ создан!");

            System.out.println("Парсим JSON документ и выводим количество станций на линиях: ");
            Parser.jsonParser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

