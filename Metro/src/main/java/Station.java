public class Station {
    private String line;
    private String name;

    public Station(String line, String name) {
        this.line = line;
        this.name = name;
    }
}
