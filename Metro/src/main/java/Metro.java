import java.util.*;

public class Metro {
    private List<Line> lines;
    private Map<String, List<String>> stations;

    public Metro(List<Line> lines, Map<String, List<String>> stations) {
        this.lines = lines;
        this.stations = stations;
    }
    public List<Line> getLines() {
        return lines;
    }
}
